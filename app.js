const app = require('express')()
const stripe = require('stripe')(process.env.SECRET_KEY)

const port = process.env.PORT || 3000

app.use(require('body-parser').urlencoded({ extended: false }))

app.post('/make-donation', (req, res) => {
  stripe.customers.create({
    email: req.body.email,
    source: req.body.id,
  })
  .then(customer =>
    stripe.charges.create({
      amount: req.body.amount,
      description: 'one time donation',
      receipt_email: req.body.email,
      currency: 'usd',
      customer: customer.id,
      metadata: {
        note: req.body.note,
      },
    })
  )
  .then(() => {
    console.log(`${req.body.email} donated $${req.body.amount / 100}!`)
    res.redirect(process.env.REDIRECT_URL)
  })
})

app.listen(port, () => {
  console.log(`Donation app running on port ${port}.`)
})
