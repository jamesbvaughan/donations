FROM node:alpine
MAINTAINER James Vaughan <james@jamesbvaughan.com>
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
CMD npm start
